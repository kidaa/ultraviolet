using System.Reflection;

[assembly: AssemblyProduct("Ultraviolet Framework")]
[assembly: AssemblyCompany("TwistedLogik Software")]
[assembly: AssemblyCopyright("Copyright � Cole Campbell 2014-2015")]

[assembly: AssemblyVersion("1.3.0.0")]
[assembly: AssemblyFileVersion("1.3.0.0")]
[assembly: AssemblyInformationalVersion("1.3.0.0 dev build")]